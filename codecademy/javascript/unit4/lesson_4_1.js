// UNIT 4: 'WHILE' LOOPS IN JAVASCRIPT
// Introduction to 'While' Loops in JS
//
// 1/11 : While we're at it

var coinFace = Math.floor(Math.random() * 2);

while(coinFace === 0){
	console.log("Heads! Flipping again...");
	var coinFace = Math.floor(Math.random() * 2);
}
console.log("Tails! Done flipping."); // Tails! Done flipping.

/******************************************************************/

// 2/11 : While syntax

var understand = true;

while( understand === true ){
	console.log("I'm learning while loops!");
	understand = false;
}
