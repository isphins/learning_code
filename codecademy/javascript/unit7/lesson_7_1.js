// Here is an array of multiples of 8. But is it correct?
var multiplesOfEight = [8, 16, 24, 32, 40, 58];

// Test to see if a number from the array is NOT a true
// multiple of eight. Real multiples will return false.
var answer = multiplesOfEight[X] % 8 !== 0;


//

for (var i = 1; i <= 20; i++) {

    if ((i % 3 === 0) && (i % 5 === 0)) {
        console.log("FizzBuzz");
    } else if (i % 5 === 0) {
        console.log("Buzz");
    } else if (i % 3 === 0) {
        console.log("Fizz");
    } else {
        console.log(i);
    }
}


//movie review
var getReview = function(movie) {

    switch (movie) {
        case 'Toy Story 2':
            return ("Great story. Mean prospector.");
            break;

        case 'Finding Nemo':
            return ("Cool animation, and funny turtles.");
            break;

        case 'The Lion King':
            return ("Great songs.");
            break;

        default:
            return ("I don't know!");
    }
};


console.log(getReview('Finding Nemo'));


//Object

var bicycle = new Object();

bicycle.speed = 0;
bicycle.gear = 1;
bicycle.frame_material = "carbon fiber";



//method

// here is bob again, with his usual properties
var bob = new Object();
bob.name = "Bob Smith";
bob.age = 30;
// this time we have added a method, setAge
bob.setAge = function(newAge) {
    bob.age = newAge;
};
// here we set bob's age to 40
bob.setAge(20);
// bob's feeling old.  Use our method to set bob's age to 20

//
var bob = new Object();
bob.age = 17;
// this time we have added a method, setAge
bob.setAge = function (newAge){
  bob.age = newAge;
};

bob.getYearOfBirth = function () {
  return 2014 - bob.age;
};
console.log(bob.getYearOfBirth());


//"This" Works for Everyone

// here we define our method using "this", before we even introduce bob
var setAge = function (newAge) {
  this.age = newAge;
};
// now we make bob
var bob = new Object();
bob.age = 30;
bob.setAge = setAge;
  
// make susan here, and first give her an age of 25
var susan = new Object();
susan.age = 25 ;
// here, update Susan's age to 35 using the method
susan.setAge = setAge ;
susan.setAge(35);

console.log(susan.age);

//Why Are Methods Important?

var bob = new Object();
bob.age = 17;
// this time we have added a method, setAge
bob.setAge = function (newAge){
  bob.age = newAge;
};

bob.getYearOfBirth = function () {
  return 2014 - bob.age;
};
console.log(bob.getYearOfBirth());

The "this" Keyword

// here we define our method using "this", before we even introduce bob
var setAge = function (newAge) {
  this.age = newAge;
};
// now we make bob
var bob = new Object();
bob.age = 30;
// and down here we just use the method we already made
bob.setAge = setAge;
  
// change bob's age to 50 here
bob.setAge(50);



// "This" Works for Everyone  ใส่ค่าให้ object ใหม่ด้วย this method

// here we define our method using "this", before we even introduce bob
var setAge = function (newAge) {
  this.age = newAge;
};
// now we make bob
var bob = new Object();
bob.age = 30;
bob.setAge = setAge;
  
// make susan here, and first give her an age of 25
var susan = new Object();
susan.age = 25 ;
// here, update Susan's age to 35 using the method
susan.setAge = setAge ;
susan.setAge(35);

console.log(susan.age);


///
Make Your Own Method
change proerty in method


var rectangle = new Object();
rectangle.height = 3;
rectangle.width = 4;
// here is our method to set the height
rectangle.setHeight = function (newHeight) {
  this.height = newHeight;
};
// help by finishing this method
rectangle.setWidth = function(newWidth){
    this.width = newWidth;
    }
  

// here change the width to 8 and height to 6 using our new methods
console.log(rectangle.height);
console.log(rectangle.width);

rectangle.setHeight(6);
rectangle.setWidth(8);

console.log(rectangle.height);
console.log(rectangle.width);



//More Kinds of Methods


var square = new Object();
square.sideLength = 6;
square.calcPerimeter = function() {
  return this.sideLength * 4;
};
// help us define an area method here
square.calcArea = function(){
    return this.sideLength * this.sideLength;
    }

var p = square.calcPerimeter();
var a = square.calcArea();




//Custom Constructors
function Person(name,age) {
  this.name = name;
  this.age = age;
}

// Let's make bob and susan again, using our constructor
var bob = new Person("Bob Smith", 30);
var susan = new Person("Susan Jordan", 25);
// help us make george, whose name is "George Washington" and age is 275
var george = new Person("George Washington" ,275);

