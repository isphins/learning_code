
// Introduction to Functions in JS
// Topic : Functions
//
/******************************************************************/
// Introducing Functions 2/13
// This is what a function looks like:

var divideByThree = function (number) {
    var val = number / 3;
    var ab = console.log(val);
};

// On line 12, we call the function by name
// Here, it is called 'dividebythree'
// We tell the computer what the number input is (i.e. 6)
// The computer then runs the code inside the function!
divideByThree(6);

/******************************************************************/
// Function syntax
// 3/13

// Below is the greeting function!
// See line 7
// We can join strings together using the plus sign (+)
// See the hint for more details about how this works.

var greeting = function (name) {
    console.log("Great to see you," + " " + name);
};

// On line 11, call the greeting function!
greeting("Art");

/******************************************************************/
// Tying it all together
// 5/13

// Nicely written function:
var calculate = function (number) {
    var val = number * 10;
    console.log(val);
}

// Badly written function with syntax errors!

var greeting =  function(name){
    console.log(name);
    } 

greeting("Art");

/******************************************************************/
// Return keyword
// 7/13

// Parameter is a number, and we do math with that parameter
var timesTwo = function(number) {
    return number * 2;
};

// Call timesTwo here!
var newNumber = timesTwo(3);
console.log(newNumber);

/******************************************************************/
// Functions, return and if / else
// 8/13

// Define quarter here.
var quarter = function(number){
    return number = number/4;
    }

if (quarter(36) % 3 === 0 ) {
  console.log("The statement is true");
} else {
  console.log("The statement is false");
}


/******************************************************************/
// Functions with two parameters
// 9/13

// Write your function starting on line 3

var perimeterBox = function(length,width){
    return length+length+width+width;
    }
    
perimeterBox(12,3);

/******************************************************************/
// Global vs Local Variables
// 10/13

var my_number = 7; //this has global scope

var timesTwo = function(number) {
 var   my_number = number * 2;
    console.log("Inside the function my_number is: ");
    console.log(my_number);
}; 

timesTwo(7);

console.log("Outside the function my_number is: ")
console.log(my_number);

// output
// Inside the function my_number is: 14
// Outside the function my_number is: 7

/******************************************************************/
// Functions recap
// 11/13

var nameString = function (name) {
	return "Hi, I am"+ " "+ name;
};
console.log(nameString("Art"));

// output : Hi, I am Art

/******************************************************************/
// Functions &  if / else 
// 12/13
// Write your function below. 
// Don't forget to call your function!

var sleepCheck = function(numHours){
    if(numHours >= 8){
        return "You\'re getting plenty of sleep! Maybe even too much!";
        }else{
            return "Get some more shut eye!";
        }
    }
    
sleepCheck(10);
sleepCheck(5);
sleepCheck(8);

// "You're getting plenty of sleep! Maybe even too much!" 

/******************************************************************/

