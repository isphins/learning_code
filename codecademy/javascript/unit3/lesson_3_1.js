//Introduction to 'For' Loops in JS
// Array positions
// 10/13
// Practice array!

var junkData = ["Eddie Murphy", 49, "peanuts", 31];

console.log(junkData[0]);
console.log(junkData[1]);
console.log(junkData[2]);
console.log(junkData[3]);

//
// Let's print out every element of an array using a for loop
// Loops and arrays I
// 11/13
var cities = ["Melbourne", "Amman", "ARTTTTT", "NYC"];

    for (var i = 0; i < cities.length; i++) {
        console.log("I would like to visit " + cities[i]);
    }
// Loops and arrays II
// 12/13
var names = ["babala","Art","akama","jadala","hamaka"];

    for(var i=0;i< names.length ; i++){
        console.log("I know someone called "+names[i]);
    }
