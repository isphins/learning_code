# Learning_code
- [Resource_script_css](https://github.com/isphins/resource_script_css)

 Leaning Code by web tutorial
 
 
 # Tutorial Begin Learning

- [:books: Freely available programming books](https://github.com/vhf/free-programming-books)
- [Codecademy](https://www.codecademy.com) >> Learning Programming
- [FreeCodeCamp open source codebase and curriculum.](https://github.com/FreeCodeCamp/FreeCodeCamp)

---

# App Framework
- [Angular.js](https://github.com/angular/angular.js) >> HTML enhanced for web apps.
- [Angular2](https://angular.io/docs/ts/latest/guide/architecture.html) >> Angular 2 is a framework to help us build client applications in HTML and JavaScript.
- [Meteor](https://github.com/meteor/meteor) >> Meteor is an ultra-simple environment for building modern web applications.
- [React](https://github.com/facebook/react) >> React is a JavaScript library for building user interfaces.
- [React Native](https://github.com/facebook/react-native) >> A framework for building native apps with React.

# CSS Reference
- [CSS Reference](http://tympanus.net/codrops/css_reference/)

# Front-End Framework
- [Bootstrap](https://github.com/twbs/bootstrap) 

# Language Programming
- [The Swift Programming Language](https://github.com/apple/swift)

# Javascript Libraries 
- [es6-cheatsheet](https://github.com/DrkSephy/es6-cheatsheet)
- [Airbnb JavaScript Style Guide()ES5,ES6 ](https://github.com/airbnb/javascript)
- [Airbnb JavaScript Style Guide()ES5,ES6 thai version](https://github.com/55146CPE/javascript-style-guide) TH

